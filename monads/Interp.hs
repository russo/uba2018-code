module Interp where


fac :: Int -> Int
fac 0 = 1
fac n = n * fac (n-1)

length1 :: [a] -> Int
length1 []     = 0
length1 (x:xs) = 1 + length1 xs


data Nat = Zero | Suc Nat

showNat :: Nat -> Int
showNat Zero      = 0
showNat (Suc nat) = 1 + showNat nat




data Expr = Con Int | Div Expr Expr

data E a = Wrong | Ok a deriving Show

interpE :: Expr -> E Int
interpE (Con num)   = Ok num
interpE (Div e1 e2) = case interpE e1 of
                        Wrong     -> Wrong
                        Ok num1   -> case interpE e2 of
                                        Wrong   -> Wrong
                                        Ok 0    -> Wrong
                                        Ok num2 -> Ok (num1 `div` num2)


data L a = Log (a,[String]) deriving Show

interpL :: Expr -> L Int
interpL (Con num)   = Log (num, ["Eval numero:"++ show num])
interpL (Div e1 e2) = case interpL e1 of
                         Log (num1, log1) ->
                              case interpL e2 of
                                   Log (num2, log2) -> Log (num1 `div` num2,
                                                           ["Se hizo DIV"]
                                                           ++ ["Left subexpression"]
                                                           ++ log1
                                                           ++ ["Right subexpression"]
                                                           ++ log2)


data I a = I a deriving Show

instance Monad I where
  return a      = I a
  (I a)  >>= f  = f a


interpI :: Expr -> I Int
interpI (Con num)   = return num
interpI (Div e1 e2) = do
  -- :: I Int
  num1 <- interpI e1
  num2 <- interpI e2
  return (num1 `div` num2)


instance Monad E where
  return a = Ok a
  Wrong  >>= f  = Wrong
  (Ok a) >>= f  = f a

todomal :: E a
todomal = Wrong

interpME :: Expr -> E Int
interpME (Con num)   = return num
interpME (Div e1 e2) = do
    num1 <- interpME e1
    num2 <- interpME e2
    if num2 == 0 then todomal
                 else return (num1 `div` num2)

instance Monad L where
  return a           = Log (a, [])
  Log (a,log1) >>= f = case f a of
                          Log (b,log2) -> Log (b,log1++log2)

logging :: String -> L ()
logging str = Log ((),[str])

interpML :: Expr -> L Int
interpML (Con num) = do
  logging ("Eval numero:"++ show num)
  return num

interpML (Div e1 e2) = do
  logging "Se hizo DIV"
  logging "Left subexpression"
  num1 <- interpML e1
  logging "Right subexpression"
  num2 <- interpML e2
  return (num1 `div` num2)












instance Functor I where
  fmap f ia = do a <- ia
                 return (f a)

instance Applicative I where
  pure x = return x
  ff <*> xs = do f <- ff
                 x <- xs
                 return (f x)


instance Functor E where
  fmap f ia = do a <- ia
                 return (f a)

instance Applicative E where
  pure x = return x
  ff <*> xs = do f <- ff
                 x <- xs
                 return (f x)


instance Functor L where
  fmap f ia = do a <- ia
                 return (f a)

instance Applicative L where
  pure x = return x
  ff <*> xs = do f <- ff
                 x <- xs
                 return (f x)
